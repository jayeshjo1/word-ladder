package assignment5;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class A5Driver
{
    public static void main(String[] args)
    {
        // Create a word ladder solver object
	int track = 0;
        Assignment5Interface wordLadderSolver = new WordLadderSolver();
        StopWatch sw = new StopWatch();
	try
	{
		FileReader fr = new FileReader("assn5data.txt");
		BufferedReader br = new BufferedReader(fr);
		String current;
		while((current = br.readLine()) != null)
		{
		    String startWord;
		    String endWord;
		    String[] splitted = current.split("\\s+");
		    if(splitted.length != 2)
		    {
			System.out.println("Invalid Input");
		    }
		    else
		    {
			startWord = splitted[0];
			endWord = splitted[1];
			try 
			{
			    sw.start();
			    List<String> result = wordLadderSolver.computeLadder(startWord, endWord);
			    sw.stop();
			    track +=1;
			    System.out.println("For the input words \"" + startWord + "\" and \"" + endWord + "\" the following word ladder was found");
			    printLadder(result); 
			    //boolean correct = wordLadderSolver.validateResult(startWord, endWord, result);

			} 
			catch (NoSuchLadderException e) 
			{
			    System.out.print(e.getMessage());
			}
			System.out.println("\n**********\n");
		    }
		    
		}
		System.out.println("total found: " + track);
		System.out.println(sw.getElapsedTime()/StopWatch.NANOS_PER_SEC + " seconds");
		br.close();
	} catch (FileNotFoundException e)
	{
	    System.out.println("Could not find file.");
	    System.out.println("System will now exit");
	    System.exit(0);
	} catch (IOException e)
	{
	    System.out.println("Could not read from file.");
	    System.out.println("System will now exit");
	    System.exit(0);
	}
    }
    
    private static void printLadder(List<String> result) {
	Iterator<String> it = result.iterator();
	System.out.print("\t");
	while(it.hasNext())
	{
	    System.out.print(it.next() + " ");
	}
    }

    // test main that doesn't read from a file
    public static void main2(String[] args)
    {
        Assignment5Interface wordLadderSolver = new WordLadderSolver();
        try
        {
           List<String> result = wordLadderSolver.computeLadder("heads", "tails");
           Iterator<String> it = result.iterator();
           while(it.hasNext())
           {
               System.out.println(it.next());
           }
        }
        catch (NoSuchLadderException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
