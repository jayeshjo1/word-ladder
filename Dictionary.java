package assignment5;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

/**
 * Creates dictionary object that utilizes HashSet. Reads from the file passed onto its constructor, otherwise default
 * "assn5words.dat" for words containing the passed number of letters, otherwise default 5. If the word starts with
 * a '*', the word is ignored. Otherwise, the sets up the HashSet with entries from the line containing the number of letters
 * specified, ignoring any other characters within each line. 
 * <br>
 * <br>
 * If the file isn't found, or there is some other problem with IO, then the System will immediately exit after printing out
 * the error. 
 * 
 * @author Jayesh, Kelsey, and Jacob
 *
 * @version 1.0
 */
public class Dictionary {
	private HashSet<String> dictionary;
	/**
	 * Allows the user to specify which file contains the list of words to be considered for the dictionary. The user
	 * must also specify the number of letters for each entry.
	 * 
	 * @param fileName - name of file to look for entries to the dictionary.
	 * @param numLetter - number of letters on each file
	 */
	Dictionary(String fileName, int numLetter)
	{
		dictionary = new HashSet<String>();
		setUpDictionary(fileName, numLetter);
	}
	/**
	 * Allows the user to create the dictionary file using the default file name "assn5words.dat" and default
	 * number of letters, 5.
	 */
	Dictionary()
	{
		dictionary = new HashSet<String>();
		String fileName = "assn5words.dat";
		int letters = 5;
		setUpDictionary(fileName,letters);
	}
	private void setUpDictionary(String fileName, int numLetter) {
		try
		{
			FileReader fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);
			String current;
			while((current = br.readLine()) != null)
			{
				if(current.charAt(0) != '*')
				{
					dictionary.add(current.substring(0, numLetter));
				}
			}
			System.out.println("Dictionary successfully read!");
			br.close();
		} catch (FileNotFoundException e)
		{
			System.out.println("Could not find " + fileName + " file.");
			System.out.println("System will now exit");
			System.exit(0);
		} catch (IOException e)
		{
			System.out.println("Could not read from " + fileName + " file.");
			System.out.println("System will now exit");
			System.exit(0);
		}
		
	}
	
	/**
	 * Allows the user to check whether the string passed is contained within the dictionary. 
	 * 
	 * @param s - string to check within the dictionary.
	 * @return true if the string is in the dictionary, false if not
	 */
	public boolean contains(String s)
	{
		return dictionary.contains(s);
	}
}
