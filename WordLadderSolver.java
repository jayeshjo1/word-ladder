/**
 * WordLadderSolver.java
 * 
 * Assignment 5
 * 
 * @see WordLadderSolver for proper javadoc.
 * 
 * @author Jayesh Joshi, Kelsey Zhu, Jacob Salazar
 */
package assignment5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * Allows the user to create a {@link Dictionary} and check to see if there exists a word ladder between two
 * words by calling {@link WordLadderSolver#computeLadder(String, String)}. 
 * <br>
 * <br>
 * The class can be instantiated to specify the file name and the number of letters contained within the dictionary
 * or, if desired, can be called with empty arguments to create the default dictionary using 5 letter words gained 
 * from the assn5words.dat file contained in the folder. If the file is not found, the System will immediately exit. 
 * <br>
 * <br>
 * The user is only able to access {@link WordLadderSolver#computeLadder(String, String)} and 
 * {@link WordLadderSolver#validateResult(String, String, List)}. The user can call compute ladder to return a list
 * that contains a word ladder from the first word to the second word. The method throws an exception with the
 * proper message if the input is not within the dictionary or there is no word ladder from the first word to the
 * last word. After the list is returned, the user can check to see if the ladder is valid by calling the validate 
 * result method.
 * 
 * 
 * @author Jayesh, Kelsey, and Jacob
 *
 */
public class WordLadderSolver implements Assignment5Interface
{
	private Dictionary dictionary;
	private int numLetters;
	private ArrayList<String> solutionList;
	private HashSet<String> checked;
	/**
	 * Allows the user to specify which file to read the dictionary from and the number of letters contained 
	 * within the dictionary file, if desired. 
	 * 
	 * @param fileName - file which contains a list of words to add onto the dictionary with the specified 
 	 * number of letters.
	 * @param numLetters - number of letters to check for words within the file. 
	 */
	WordLadderSolver(String fileName, int numLetters)
	{
		this.numLetters = numLetters;
		dictionary = new Dictionary(fileName, numLetters);

	}
	/**
	 * Allows the user to not specify the file to read the dictionary. Will try to create a new {@link Dictionary} 
	 * instance by reading from the default assn5words.dat file and with 5 letters per word limitation.
	 */
	WordLadderSolver()
	{
		numLetters = 5;
		dictionary = new Dictionary();
	}

	
    @Override
    // Did not write javadoc because apparently uses the one in the interface! Good to know!
    public List<String> computeLadder(String startWord, String endWord) throws NoSuchLadderException 
    {
        solutionList = new ArrayList<>();
        checked = new HashSet<>();
        if(startWord.equals(endWord))
        {
        	throw new NoSuchLadderException("The input words are equal thus there is no word ladder between the two!");
        }
        else if(!exists(startWord, endWord))
        {
        	throw new NoSuchLadderException("At least one of the words " + startWord + " and " + endWord + " are not legitimate " + numLetters + "-letter words from the dictionary");
        }
        else {
        	if(makeLadder(startWord, endWord, -1))
        	{
            	return solutionList;	
        	}
        	else
        	{
        		throw new NoSuchLadderException("There is no word ladder between " + startWord + " and " + endWord + "!");
        	}
        }
    }

    @Override
    public boolean validateResult(String startWord, String endWord, List<String> wordLadder) 
    {
	// if wordLadder's first and last word does not match start word and endWord, false!
    	if(wordLadder.get(0) != startWord || wordLadder.get(wordLadder.size() - 1) != endWord)
    	{
    	    return false;
    	}
    	else 
    	{
    	    String current;
    	    String next;
    	    Iterator<String> it = wordLadder.iterator();
    	    current = it.next();
    	    while(it.hasNext())
    	    {
    		next = it.next();
    		if(difference(current,next) != 1 && !dictionary.contains(current) && !dictionary.contains(next))
    		{
    		    return false;
    		}
    		current = next;
    	    }
    	    return true;
    	}
    }
    
    // used to see if both words exists within the dictionary
    private boolean exists(String fromWord, String toWord)
    {
    	return dictionary.contains(fromWord) && dictionary.contains(toWord);
    }
    
    /**
     * Dear Jo,
     * 
     * Last time you said no need to add javadocs for private methods, is this the way to do it then?
     * Actually, talked to someone, said its okay to put javadocs just as a reference for yourself, but meh!
     */
    // Step 1: get the correct index
    // Step 2: calculate difference to see if already 1, if so add final and return true!
    // Step 3: go through all valid indexes and add to local list containing all words with 1 letter difference
    // Step 4: Sort and see if there is any words with only 1 difference, if so, add final and return true!
    // Step 5: Go through the local list to see if there exists a ladder from one of the words found.
    // Step 6: If not found, then remove the current word from solution List and return false!
    private boolean makeLadder(String fromWord, String toWord, int prevIndex)
    {
    	ArrayList<String> currentList = new ArrayList<>();
    	// Step 1
    	int index;
    	if(prevIndex == 0)
    	{
    		index = 1;
    	}
    	else 
    	{
    		index = 0;
    	}
    	solutionList.add(fromWord);
    	checked.add(fromWord);
    	
    	// Step 2
    	int diff = difference(fromWord,toWord);
    	if(diff == 1)
    	{
    		solutionList.add(toWord);
    		return true;
    	}
    	
    	// Step 3
    	while(index < numLetters)
    	{
    		currentList.addAll(makeList(fromWord, toWord, index));
    		index += 1;
    		if(index == prevIndex)
    		{
    			index +=1;
    		}
    	}
    	
    	// Step 4
   	Collections.sort(currentList);
	if(currentList.size() != 0)
	{
		if(currentList.get(0).charAt(0) == '1')
		{
			solutionList.add(currentList.get(0).substring(2,currentList.get(0).length()));
			solutionList.add(toWord);
			return true;
		}
	}
	
	// Step 5
	Iterator<String> it = currentList.iterator();
	while(it.hasNext())
	{
		String temp = it.next();
		if(!checked.contains(temp.substring(2,  temp.length())))
		{
		    if(makeLadder(temp.substring(2, temp.length()), toWord, Integer.parseInt(temp.substring(1,2))))
		    {
			return true;
		    }
		}
		    
	}
	
	// Step 6
    	solutionList.remove(solutionList.size() - 1);
    	return false;
    }
    
    // calculate number of different letters from the first word to the secnod
    private int difference(String fromWord, String toWord)
    {
    	int result = 0;
    	for(int k = 0; k < fromWord.length(); k +=1 )
    	{
    		if(fromWord.charAt(k) != toWord.charAt(k))
    		{
    			result +=1;
    		}
    	}
    	return result;
    }
    
    // First find a word with one different letter in the given index and see if it exists in dictionary
    // Then, create a list that contains two prepends:
    // Index 0: contains the difference between fromWord to toWord
    // Index 1: contains the index (to use to call makeLadder again)
    private ArrayList<String> makeList(String fromWord, String toWord, int index)
    {
	StringBuilder temp = new StringBuilder(fromWord);
	temp.insert(0, Integer.toString(0));
	temp.insert(0, Integer.toString(0));
    	ArrayList<String> result = new ArrayList<>();
    	char current = 'a';
    	if(current == fromWord.charAt(index))
		{
			current += 1;
		}
    	while(current <= 'z')
    	{
    		temp.replace(index+2, index +3, Character.toString(current));
    		String tempString = temp.toString().substring(2, temp.length());
    		if(dictionary.contains(tempString) && !solutionList.contains(tempString))
    		{
    			temp.replace(1, 2, Integer.toString(index));
    			temp.replace(0, 1, Integer.toString(difference(tempString, toWord)));
    			result.add(temp.toString());
    		}
    		current +=1;
    		if(current == fromWord.charAt(index))
    		{
    			current += 1;
    		}
    	}
    	return result;
    }
}
